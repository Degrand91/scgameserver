//var express = require('express');
var MongoClient = require('mongodb').MongoClient;

module.exports.run = function (worker) {
    var debugFlag = true;
    if(debugFlag) console.log(' >> Worker PID:', process.pid);


    function startServer () {
        var matchingQueue = [];
        var rooms = {};
        var usersData = {};
        var scServer = worker.scServer;
        var url = 'mongodb://ip:27017/db';


        MongoClient.connect(url, function(err, db) {
            if (err) throw  err;
            socketInitiate(db);
            setInterval(matchPlayers,500);

            /*
            var cursor =db.collection('equip').find( );
            cursor.each(function(err, doc) {
                console.log(doc);
            })
            db.close();
            */
            if (debugFlag) console.log(new Date().toUTCString(),"|","Connected correctly to MongoDB.");
        });

        function socketInitiate(db){
            scServer.on('connection', connection);
            function connection (socket) {
                socket.on("disconnect", disconnect);
                socket.on("register", register);
                socket.on("getUserData", getUserData);
                socket.on("joinQueue", joinQueue);
                socket.on("leaveQueue", leaveQueue);
                socket.on("joinRoom", joinRoom);
                socket.on("leaveRoom", leaveRoom);

                function disconnect () {
                    if (debugFlag) console.log(new Date().toUTCString(),"|","player disconnect ",socket.username);
                    var channels = socket.subscriptions();
                    channels.forEach(function(channel){
                        if (channel === "matchingQueue" ) leaveQueue();
                        if (channel.match(/room-/gi)) leaveRoom(channel);
                    });
                }
                function register (username) {
                    socket.username = username;
                    if (debugFlag) console.log(new Date().toUTCString(),"|","player connected ",socket.username);
                }
                function getUserData(username){
                    db.collection("users").findOne({token:"token"},{"_id":0,"username":1,"status":1,"type":1,"slots":1},function(err,doc){
                        doc.username = username;
                        usersData[username] = doc;
                        socket.emit("setUserData",usersData[username]);
                        if (debugFlag) console.log(new Date().toUTCString(),"|","player userData requested ",socket.username);
                    });
                }

                function joinQueue () {
                    if (matchingQueue.indexOf(socket.username) >= 0) return;
                    matchingQueue.push(socket.username);
                    scServer.exchange.publish(socket.username,{event:"joinedQueue"});
                    if (debugFlag) console.log(new Date().toUTCString(),"|","player join queue ",socket.username);
                }
                function leaveQueue () {
                    var index = matchingQueue.indexOf(socket.username);
                    if (index >= 0)  matchingQueue.splice(index,1);
                    scServer.exchange.publish(socket.username,{event:"leftQueue"});
                    if (debugFlag) console.log(new Date().toUTCString(),"|","player left queue ",socket.username);
                }

                function joinRoom (roomId) {
                    var data = {};
                    data[socket.username] = usersData[socket.username];
                    scServer.exchange.publish(roomId,{event:"playerJoined",data:data});
                    rooms[roomId].players[socket.username] = data[socket.username];
                    scServer.exchange.publish(roomId,{event:"updateRoom",data:rooms[roomId]});
                }
                function leaveRoom (roomId){
                    var room = rooms[roomId];
                    room.status = "close";
                    scServer.exchange.publish(roomId,{event:"leaveRoom",data:room});
                }
            }
        }
        function matchPlayers (){
            if (matchingQueue.length < 2) return;
            var room = new Room("room-"+new Date()*1000+parseInt(Math.random()*10));
            var playerId1 = matchingQueue[Math.floor(Math.random()*matchingQueue.length)];
            var matchingQueueTemp = matchingQueue.filter(function(obj) { return [playerId1].indexOf(obj) === -1; });
            var playerId2 = matchingQueueTemp[Math.floor(Math.random()*matchingQueueTemp.length)];
            if (!playerId1 || !playerId2) return null;
            room.players[playerId1] = usersData[playerId1];
            room.players[playerId2] = usersData[playerId2];
            rooms[room.id] = room;
            scServer.exchange.publish(playerId1,{event:"setRoom",data:room});
            scServer.exchange.publish(playerId2,{event:"setRoom",data:room});
            matchingQueue.splice(matchingQueue.indexOf(playerId1),1);
            matchingQueue.splice(matchingQueue.indexOf(playerId2),1);
            if (debugFlag) console.log(new Date().toUTCString(),"|","players matched ",room.id, playerId1,playerId2);
        }
        function Room(roomId) {
            this.id = roomId.toString();
            this.players = {};
            this.status = "open";            // 1 open, 2 inGame, 3 end
            this.score = {
                red: 0,
                blue:0
            };
        }
    }
    startServer();
};

